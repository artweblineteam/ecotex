		<div class="mobile-header sticky-mobile">
			<div class="container">
				<div class="logo">
					<a href="/">
						<img src="assets/img/logo.png" alt="logo Ecotex">
					</a>
				</div>
				<div class="mobile-menu">
					<div class="hamb-menu-wrap">
						<span class="hamburger-icon"></span>
					</div>
					<div class="open-mobile-menu hide">
						<nav class="nav">
							<ul>
								<li class="nav-first">
									<div class="nav-first-mobile">
										<a href="http://google.com" class="menu-title-mobile">Постельное белье</a>
									</div>
									<div class="nav-second-mobile">
										<div class="mobile-submenu">
											<ul class="mobile-submenu-items">
												<li><a href="#">Сатин-премиум</a></li>
												<li><a href="#">Сатин-жаккард</a></li>
												<li><a href="#">Сатин 3D</a></li>
												<li><a href="#">Сатин-комфорт</a></li>
												<li><a href="#">Поплин</a></li>
											</ul>
										</div>
									</div>
								</li>           
								<li class="nav-first">
									<div class="nav-first-mobile">
										<a href="#" class="menu-title-mobile">Подушки</a>
									</div>
									<div class="nav-second-mobile">
										<div class="mobile-submenu">
											<ul class="mobile-submenu-items">
												<li><a href="#">Сатин-премиум</a></li>
												<li><a href="#">Сатин-жаккард</a></li>
												<li><a href="#">Сатин 3D</a></li>
												<li><a href="#">Сатин-комфорт</a></li>
												<li><a href="#">Поплин</a></li>
											</ul>
										</div>
									</div>
								</li>      
								<li class="nav-first">
									<div class="nav-first-mobile">
										<a href="#" class="menu-title-mobile">Одеяла</a>
									</div>
									<div class="nav-second-mobile">
										<div class="mobile-submenu">
											<ul class="mobile-submenu-items">
												<li><a href="#">Сатин-премиум</a></li>
												<li><a href="#">Сатин-жаккард</a></li>
												<li><a href="#">Сатин 3D</a></li>
												<li><a href="#">Сатин-комфорт</a></li>
												<li><a href="#">Поплин</a></li>
											</ul>
										</div>
									</div>
								</li>      
								<li class="nav-first">
									<div class="nav-first-mobile">
										<a href="#" class="menu-title-mobile">Наматрасники</a>
									</div>
									<div class="nav-second-mobile">
										<div class="mobile-submenu">
											<ul class="mobile-submenu-items">
												<li><a href="#">Сатин-премиум</a></li>
												<li><a href="#">Сатин-жаккард</a></li>
												<li><a href="#">Сатин 3D</a></li>
												<li><a href="#">Сатин-комфорт</a></li>
												<li><a href="#">Поплин</a></li>
											</ul>
										</div>
									</div>
								</li>      
								<li class="nav-first">
									<div class="nav-first-mobile">
										<a href="#" class="menu-title-mobile">Пледы</a>
									</div>
									<div class="nav-second-mobile">
										<div class="mobile-submenu">
											<ul class="mobile-submenu-items">
												<li><a href="#">Сатин-премиум</a></li>
												<li><a href="#">Сатин-жаккард</a></li>
												<li><a href="#">Сатин 3D</a></li>
												<li><a href="#">Сатин-комфорт</a></li>
												<li><a href="#">Поплин</a></li>
											</ul>
										</div>
									</div>	
								</li> 
								<li class="nav-first">
									<div class="nav-first-mobile">
										<a href="#" class="menu-title-mobile">Махровые изделия</a>
									</div>
									<div class="nav-second-mobile">
										<div class="mobile-submenu">
											<ul class="mobile-submenu-items">
												<li><a href="#">Сатин-премиум</a></li>
												<li><a href="#">Сатин-жаккард</a></li>
												<li><a href="#">Сатин 3D</a></li>
												<li><a href="#">Сатин-комфорт</a></li>
												<li><a href="#">Поплин</a></li>
											</ul>
										</div>
									</div>
								</li>
							</ul>
						</nav>
						<div class="top-links">
							<ul>
								<li><a href="#">О компании</a></li> 
								<li><a href="#">Где купить</a></li>
								<li><a href="#">Контакты</a></li>
							</ul>
						</div>
						<div class="top-info">
							<a href="" class="tel">+7 (495) 225-34-53</a>
							<a href="" class="tel">8-800-225-34-53</a>
							<span class="info text-color-light">Работаем с 10 до 21</span>
						</div>
						<div class="reg-links">
							<div class="sign-up">
								<a href="#">Регистрация</a>
							</div>
							<div class="sign-in">
								<a href="#">Личный кабинет</a>
							</div>
						</div>
					</div>
				</div>
				<div class="total-cart">
					<a href="#" class="cart-content">
						<span class="cart-item">
							<i class="icon-cart"></i>
						</span>
						<span class="cart-details">0</span>
					</a>
					<div class="cart-box">
						<div class="cart-box-content">
							<span class="shoping-cart-content"> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
							<div class="shopping-buttons">
								<button class="btn">Просмотреть корзину</button>
								<button class="btn">Оформить заказ</button>
							</div>
						</div>
					</div>
				</div>
				<div class="search">
					<i class="icon-search"></i>
					<div class="search-line">
						<div class="container">
							<div class="row">
								<form class="search-form" action="#">
									<input type="text" name="search" placeholder="Поиск по сайту, введите, что бы Вы хотели найти">
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php include ('components/callback-mobile-header.php') ?>	
			</div>
		</div>