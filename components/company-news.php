<!-- #company-news -->
	<section id="company-news">
		<div class="container">
			<div class="row">
				<div class="section-titles">
					<h2>Новости компании</h2>
					<a href="#" class="extra-link">Посмотреть все новости</a>
				</div>
				<div class="news-item col-md-4">
					<a href="#" class="news-thumb thumb-link">
						<img src="assets/img/thumb/news1.png" alt="news thumb">
					</a>
					<a href="#" class="news-meta">
						Июль 5, 2018
					</a>
					<a href="#" class="news-title">
						Дипломат – новая коллекция полотенец
					</a>
				</div>
				<div class="news-item col-md-4">
					<a href="#" class="news-thumb thumb-link">
						<img src="assets/img/thumb/news2.png" alt="news thumb">
					</a>
					<a href="#" class="news-meta">
						Июнь 22, 2018
					</a>
					<a href="#" class="news-title">
						Новое поступление пледов
					</a>
				</div>
				<div class="news-item col-md-4">
					<a href="#" class="news-thumb thumb-link">
						<img src="assets/img/thumb/news3.png" alt="news thumb">
					</a>
					<a href="#" class="news-meta">
						Май 24, 2018
					</a>
					<a href="#" class="news-title">
						История шотландки
					</a>
				</div>
			</div>
		</div>
	</section>
	<!-- #company-news END -->

	<div class="del"></div>