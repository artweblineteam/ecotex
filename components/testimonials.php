<!-- #testimonials -->
	<section id="testimonials">
		<div class="container">
			<div class="row">
				<div class="section-titles">
					<h2>Отзывы наших клиентов</h2>
					<a href="#" class="extra-link">Посмотреть все отзывы</a>
				</div>
				<div class="col-md-12">
					<div class="testimonials-carousel">
						<div class="testimonials-item">
							<div class="testimonials-text">
								<span>Наша компания нашла для себя постоянного поставщика текстильной продукции в лице компании Экотекс, которую отличает отменное качество продукции и непревзойденный сервис!
								</span>
							</div>
							<div class="testimonials-meta">
								<span class="testimonials-name">Петр Ивано</span>
								<span class="testimonials-job">Генеральный директор</span>
							</div>
							<div class="testimonials-logo">
								<img src="assets/img/icons/company-testim1.png">
							</div>
						</div>
						<div class="testimonials-item">
							<div class="testimonials-text"><span>Мы ежемесячно продаем продукции компании Экотекс на несколько миллионов рублей, что говорит само за себя о ее качестве и надежности!</span></div>
							<div class="testimonials-meta">
								<span class="testimonials-name">Ярослав Басов</span>
								<span class="testimonials-job">Генеральный директор</span>
							</div>
							<div class="testimonials-logo">
								<img src="assets/img/icons/company-testim2.png">
							</div>
						</div>
						<div class="testimonials-item">
							<div class="testimonials-text"><span>Наша компания нашла для себя постоянного поставщика текстильной продукции в лице компании Экотекс, которую отличает отменное качество продукции и непревзойденный сервис!</span></div>
							<div class="testimonials-meta">
								<span class="testimonials-name">Инна Яценко</span>
								<span class="testimonials-job">Генеральный директор</span>
							</div>
							<div class="testimonials-logo">
								<img src="assets/img/icons/company-testim1.png">
							</div>
						</div>
						<div class="testimonials-item">
							<div class="testimonials-text"><span>Мы ежемесячно продаем продукции компании Экотекс на несколько миллионов рублей, что говорит само за себя о ее качестве и надежности!</span></div>
							<div class="testimonials-meta">
								<span class="testimonials-name">Ярослав Басов</span>
								<span class="testimonials-job">Генеральный директор</span>
							</div>
							<div class="testimonials-logo">
								<img src="assets/img/icons/company-testim2.png">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- #testimonials END -->

	<div class="del"></div>