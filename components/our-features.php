<!-- #our-features -->
	<section id="our-features">
		<div class="container">
			<div class="row">
					<div class="col-md-4">
						<div class="features-item">
							<div class="features-icon">
								<img src="assets/img/icons/icon-tube.png" alt="icon">
							</div>
							<div class="features-text">
								<h3>Собственное производство</h3>
								<ul class="features-list">
									<li class="features-list-item">Минимальные сроки производства продукции</li>
									<li class="features-list-item">Цены производителя</li>
									<li class="features-list-item">Управление качеством</li>
								</ul>
							</div>
						</div>
						<div class="features-item">
							<div class="features-icon">
								<img src="assets/img/icons/icon-layers.png" alt="icon">
							</div>
							<div class="features-text">
								<h3>Вакуумизация продукции</h3>
								<ul class="features-list">
									<li class="features-list-item">Экономия места при складировании</li>
									<li class="features-list-item">Экономия материальных затрат при транспортировке</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="features-item">
							<div class="features-icon">
								<img src="assets/img/icons/icon-bag.png" alt="icon">
							</div>
							<div class="features-text">
								<h3>Личный кабинет</h3>
								<ul class="features-list">
									<li class="features-list-item">Актуальные остатки</li>
									<li class="features-list-item">Возможность круглосуточных заказов</li>
									<li class="features-list-item">История заказов</li>
									<li class="features-list-item">Статус заказов</li>
								</ul>
							</div>
						</div>
						<div class="features-item">
							<div class="features-icon">
								<img src="assets/img/icons/icon-code.png" alt="icon">
							</div>
							<div class="features-text">
								<h3>EAN code</h3>
								<ul class="features-list">
									<li class="features-list-item">Полное штрихкодирование продукции - быстрота приема и отгрузки</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="features-item">
							<div class="features-icon">
								<img src="assets/img/icons/icon-master.png" alt="icon">
							</div>
							<div class="features-text">
								<h3>Экспресс отгрузка</h3>
								<ul class="features-list">
									<li class="features-list-item">Отгрузка в течении 1-2-3 дней от получения заказа</li>
								</ul>
							</div>
						</div>
					</div>
			</div>
		</div>
	</section>
<!-- #our-features END -->

	<div class="del"></div>