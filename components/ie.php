<?php ?>
<div class="ie-overlay"></div>
<div class="ie-notification">
	<h4>Вы используете устаревшую версию браузера</h4>
	<p>Для корректной работы сайта обновите ваш браузер</p>
	<a href="https://www.microsoft.com/en-us/download/internet-explorer.aspx" class="btn"> Oбновить</a>
	<small>Рекомендуемые браузеры: 
		<a href="https://www.google.ru/chrome/browser/desktop/">Google Chrome</a>, 
		<a href="https://www.mozilla.org/ru/firefox/new/">Firefox</a>, 
		<a href="http://www.opera.com/ru/computer/windows">Opera</a>
	</small>
</div>