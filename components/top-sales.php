<!-- #top-sales -->
	<section id="top-sales">
		<div class="container">
			<div class="row">
				<div class="section-titles">
					<h2>Хиты продаж</h2>
					<a href="#" class="extra-link">Посмотреть все хиты</a>
				</div>
				<div class="top-sales-products">
					<div class="top-sales-item col-md-4">
						<a href="#" class="thumb-link">
							<img src="assets/img/thumb/top-sales1.png" alt="top sales thumb">
						</a>
						<a href="#" class="category-links">Кашемир</a>
					</div>
					<div class="top-sales-item col-md-4">
						<a href="#" class="thumb-link">
							<img src="assets/img/thumb/top-sales2.png" alt="top sales thumb">
						</a>
						<a href="#" class="category-links">Глория</a>
					</div>
					<div class="top-sales-item col-md-4">
						<a href="#" class="thumb-link">
							<img src="assets/img/thumb/top-sales3.png" alt="top sales thumb">
						</a>
						<a href="#" class="category-links">Косточки</a>
					</div>
				</div>
			</div>
		</div>		
	</section>
<!-- #top-sales END -->

	<div class="del"></div>