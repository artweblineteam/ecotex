	<!-- #our-products -->
	<section id="our-products">
		<div class="container">
			<div class="row">
				<div class="section-titles">
					<h2>Наша продукция</h2>
					<a href="#" class="extra-link">Посмотреть всю продукцию</a>
				</div>
				<div class="our-products-item col-md-4">
					<a href="#" class="thumb-link">
						<img src="assets/img/thumb/our-product1.png" alt="our products thumb">
					</a>
					<a href="#" class="category-links">Постельное белье</a>
				</div>
				<div class="our-products-item col-md-4">
					<a href="#" class="thumb-link">
						<img src="assets/img/thumb/our-product2.png" alt="our products thumb">
					</a>
					<a href="#" class="category-links">Подушки</a>
				</div>
				<div class="our-products-item col-md-4">
					<a href="#" class="thumb-link">
						<img src="assets/img/thumb/our-product3.png" alt="our products thumb">
					</a>
					<a href="#" class="category-links">Одеяла</a>
				</div>
				<div class="our-products-item col-md-4">
					<a href="#" class="thumb-link">
						<img src="assets/img/thumb/our-product4.png" alt="our products thumb">
					</a>
					<a href="#" class="category-links">Наматрасники</a>
				</div>
				<div class="our-products-item col-md-4">
					<a href="#" class="thumb-link">
						<img src="assets/img/thumb/our-product5.png" alt="our products thumb">
					</a>
					<a href="#" class="category-links">Пледы</a>
				</div>
				<div class="our-products-item col-md-4">
					<a href="#" class="thumb-link">
						<img src="assets/img/thumb/our-product6.png" alt="our products thumb">
					</a>
					<a href="#" class="category-links">Махровые изделия</a>
				</div>
			</div>
		</div>
	</section>
	<!-- #our-products END -->

	<div class="del"></div>