<!-- #about-category -->
	<section id="about-category">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<h2>Качественный текстиль: одеяло оптом от производителя</h2>
				</div>
				<div class="seo-text col-md-10 col-md-offset-1">
					<p>Торгово-производственная компания «Экотекс» является отечественным производителем домашнего текстиля, успешно работает и динамично развивается на российском рынке.</p>
					<p>Качество, доступные цены, стиль и актуальность – вот составляющие популярной торговой марки Ecotex на российском текстильном рынке. Мы рады познакомить Вас с разнообразным ассортиментом изделий, которые мы производим: комплекты постельного белья, одеяла оптом, подушки, наматрасники, полотенца оптом, текстиль для кухни, покрывала. Разнообразные наполнители в изделиях: от пухоперовых, шерстяных, хлопковых до полиэфирных волокон нового поколения.</p>
					<p>Надежность, доступные цены, стиль и актуальность, яркость и оригинальность, качество — вот составляющие популярности торговой марки Ecotex на текстильном рынке. Каждое изделие — это отдельное произведение искусства. Экотлогичность и натуральность тканей, оригинальность цветовых решений, разнообразие наполнителей от натуральных до синтетических, широкий ассортимент изделий, эффектная упаковка — все подчеркивает всепоглощающее чувство комфорта и практичность наших изделий.</p>
				</div>
			</div>
		</div>
	</section>
<!-- #about-category END -->