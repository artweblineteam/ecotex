<!-- .category-products -->	
	<div class="category-products">
		<div class="category-product-item col-md-4">
			<ul class="label">
				<li class="hot">hot</li>
				<li class="new">new</li>
				<li class="sale">sale</li>
			</ul>
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat1.jpg" alt=""></a>
			<a href="#" class="product-links">Бамбук Royal классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat2.jpg" alt=""></a>
			<a href="#" class="product-links">Кашемир классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat3.jpg" alt=""></a>
			<a href="#" class="product-links">Меринос классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat4.jpg" alt=""></a>
			<a href="#" class="product-links">Файбер-Комфорт классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat5.jpg" alt=""></a>
			<a href="#" class="product-links">Лебяжий пух классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat6.jpg" alt=""></a>
			<a href="#" class="product-links">Сафари классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat7.jpg" alt=""></a>
			<a href="#" class="product-links">Алоэ вера классическое</a>
		</div>
		<div class="category-product-item label-hot col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat8.jpg" alt=""></a>
			<a href="#" class="product-links">Золотое руно облегченное</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat9.jpg" alt=""></a>
			<a href="#" class="product-links">Меринос классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat10.jpg" alt=""></a>
			<a href="#" class="product-links">Бамбук Royal классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat11.jpg" alt=""></a>
			<a href="#" class="product-links">Кашемир классическое</a>
		</div>
		<div class="category-product-item col-md-4">
			<a href="#" class="category-product-thumb"><img src="assets/img/thumb/cat12.jpg" alt=""></a>
			<a href="#" class="product-links">Меринос классическое</a>
		</div>
	</div>
<!-- .category-products END -->	