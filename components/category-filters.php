<!-- .category-filters -->
	<div class="category-filters">
		<div class="col-md-8">
			<span class="filters-title">Фильтры</span>
			<select class="filter" id="size-filter">
				<option>Размер</option>
				<option>2-спальный макс (чемодан)</option>
				<option>110 × 140 см</option>
				<option>130 × 180 см</option>
				<option>160 × 200 см</option>
				<option>180 × 200 см</option>
				<option>200 × 200 см</option>
				<option>200 × 220 см</option>
			</select>
			<select class="filter" id="filler-filter">
				<option>Наполнитель</option>
				<option>Пух</option>
				<option>Файбер</option>
				<option>Силикон</option>
				<option>Шерсть</option>
			</select>
			<select class="filter" id="collections-filter">
				<option>Коллекция</option>
				<option>Роял</option>
				<option>Премиум</option>
				<option>Комфорт</option>

			</select>
		</div>
		<div class="col-md-4">
			<span class="filters-title">Сортировка по</span>
			<select class="filter" id="sort-by-filter">
				<option>Возрастанию цены</option>
				<option>Убыванию цены</option>
				<option>Популярности</option>
			</select>
		</div>
	</div>
<!-- .category-filters END -->	