<!-- #product-data-gallery -->
	<div class="col-md-7" id="product-data-gallery">
		<div class="product-gallery">
			<div class="slider-for"> 
				<div class="slide slide-product">
					<img src="assets/img/thumb/big-pr1.jpg">
				</div>
				<div class="slide slide-product">
					<img src="assets/img/thumb/big-pr3.jpg">
				</div>
				<div class="slide slide-product">
					<img src="assets/img/thumb/big-pr1.jpg">
				</div>
				<div class="slide slide-product">
					<img src="assets/img/thumb/big-pr3.jpg">
				</div>
			</div>

			<div class="slider-nav">
				<div class="thumb-slide"><img src="assets/img/thumb/big-pr1.jpg"></div>
				<div class="thumb-slide"><img src="assets/img/thumb/big-pr3.jpg"></div>
				<div class="thumb-slide"><img src="assets/img/thumb/big-pr1.jpg"></div>
				<div class="thumb-slide"><img src="assets/img/thumb/big-pr3.jpg"></div>
			</div>
		</div>
	</div>
<!-- #product-data-gallery END -->