<!-- #product-collections -->
	<section id="product-collections">
		<div class="container">
			<div class="row">
				<div class="section-titles">
					<h2>Одеяла коллекции <b>Royal</b></h2>
					<a href="#" class="extra-link">Посмотреть всю коллекцию</a>
				</div>
				<div class="top-sales-products">
					<div class="top-sales-item col-md-4">
						<a href="#" class="thumb-link">
							<img src="assets/img/thumb/collections1.png" alt="top sales thumb">
						</a>
						<a href="#" class="category-links">Меринос классическое</a>
					</div>
					<div class="top-sales-item col-md-4">
						<a href="#" class="thumb-link">
							<img src="assets/img/thumb/collections2.png" alt="top sales thumb">
						</a>
						<a href="#" class="category-links">Кашемир классическое</a>
					</div>
					<div class="top-sales-item col-md-4">
						<a href="#" class="thumb-link">
							<img src="assets/img/thumb/collections3.png" alt="top sales thumb">
						</a>
						<a href="#" class="category-links">Сафари классическое</a>
					</div>
					<div class="top-sales-item col-md-4">
						<a href="#" class="thumb-link">
							<img src="assets/img/thumb/collections2.png" alt="top sales thumb">
						</a>
						<a href="#" class="category-links">Кашемир классическое</a>
					</div>
				</div>
			</div>
		</div>		
	</section>
<!-- #product-collections END -->