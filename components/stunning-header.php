<!-- #stunning-header -->
<section id="stunning-header">
	<div class="container">
		<div class="row">
			<div class="breadcrumbs">
				<ul class="breadcrumbs-navigation">
					<li class="breadcrumbs-link"><a href="#">Экотекс</a></li>
					<li class="breadcrumbs-link"><a href="#">Одеяла</a></li>
					<li class="breadcrumbs-link"><a href="#">Премиум</a></li>
				</ul>
			</div>
			<div class="page-title">
				<h1>Одеяла</h1>
			</div>
		</div>
	</div>
</section>
<!-- #stunning-header END -->