<!-- #home-slider -->
	<section id="home-slider">
		<div class="container">
			<div class="row">
				<div class="slider">
					<div class="slider-item">
						<img src="assets/img/slider/slider.jpg">
						<h1 class="slider-title">Всем подарки <br>и скидки!</h1>
					</div>
					<div class="slider-item">
						<img src="assets/img/slider/slider.jpg">
						<h1 class="slider-title">Всем скидки <br>и подарки!</h1>
					</div>
					<div class="slider-item">
						<img src="assets/img/slider/slider.jpg">
						<h1 class="slider-title">Акционные <br>предложения!</h1>
					</div>
					<div class="slider-item">
						<img src="assets/img/slider/slider.jpg">
						<h1 class="slider-title">Домашний <br>текстиль!</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- #home-slider END -->