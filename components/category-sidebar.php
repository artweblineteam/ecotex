<!-- .sidebar -->
	<div class="col-md-3 sidebar">
		<div class="row">
			<!-- .side-category-bar -->
			<div class="side-category-bar">
				<h4>Категории</h4>
				<!-- .category-widget -->
				<div class="category-widget">
					<h6 class="category-widget-title">
						<a href="http://google.com"><span>Постельное белье</span></a>
					</h6>
					<div class="category-block active">
						<ul class="cw-small">
							<li>Роял</li>
							<li>Премиум</li>
							<li>Комфорт</li>
							<li>Комфорт</li>
							<li>Роял</li>
						</ul>
					</div>
					<h6 class="category-widget-title">
						<a href="#"><span>Подушки</span></a>
					</h6>
					<div class="category-block">
						<ul class="cw-small">
							<li>Роял</li>
							<li>Премиум</li>
							<li>Премиум</li>	
						</ul>
					</div>
					<h6 class="category-widget-title">
						<a href="#"><span>Одеяла</span></a>
					</h6>
					<div class="category-block">
						<ul class="cw-small">
							<li>Роял</li>
							<li>Премиум</li>
						</ul>
					</div>
					<h6 class="category-widget-title">
						<a href="#"><span>Наматрасники</span></a>
					</h6>
					<div class="category-block">
						<ul class="cw-small">
							<li>Роял</li>
							<li>Премиум</li>
							<li>Комфорт</li>
						</ul>
					</div>
					<h6 class="category-widget-title">
						<a href="#"><span>Пледы</span></a>
					</h6>
					<div class="category-block">
						<ul class="cw-small">
							<li>Роял</li>
							<li>Премиум</li>
							<li>Комфорт</li>
						</ul>
					</div>
					<h6 class="category-widget-title">
						<a href="#"><span>Махровые изделия</span></a>
					</h6>
					<div class="category-block">
						<ul class="cw-small">
							<li>Роял</li>
							<li>Премиум</li>
							<li>Комфорт</li>
						</ul>
					</div>
				</div>
				<!-- .category-widget END -->

				<!-- .collections-widget -->
				<div class="collections-widget">
					<h4>Наши коллекции</h4>
					<ul>
						<li><a href="">Роял</a></li>
						<li><a href="">Премиум</a></li>
						<li><a href="">Комфорт</a></li>
					</ul>
				</div>	
				<!-- .collections-widget END-->
				<!-- .side-banner -->
				<div class="side-banner">

				</div>
				<!-- .side-banner END -->
			</div>
			<!-- .side-category-bar END -->	
		</div>
	</div>
<!-- .sidebar END