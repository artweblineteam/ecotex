<?php ?>
<div class="callback-popup modal-box" id="callbackPopup">
	<a href="#" class="x-form-button js-modal-close">×</a>

		 <form id="callbackForm" class="callback-form" action="#" method="#">
		 	<h4>Закажите обратный звонок</h4>
			<div class="name-field form-fields">
				<label class="text-color-light">Имя</label>
				<input type="text" name="Name" required="required">
			</div>

			<div class="phone-field form-fields">
				<label class="text-color-light">Телефон</label>
				<input type="text" name="Code" required class="country-code" id="countryCode">
				<input type="text" name="Phone" required class="phone-numb" id="phoneNumb">
			</div>

			<button type="submit" class="send-form">Заказать звонок</button>
			
		</form>
	</div>
<div class="top-callback">
	<a href="#" class="js-open-modal" data-modal-id="callbackPopup" >Заказать звонок </a>
	<span class="info text-color-light">Перезвоним в течении 5 минут</span>
</div>