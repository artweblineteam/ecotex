<!-- #product-data-details -->
	<div class="col-md-5" id="product-data-details">
		<div class="product-details">
			<h3 class="product-title">Одеяло Бамбук <br>Royal классическое</h3>
			<div class="category-collections">
				<span class="subtitile">Коллекция:</span>
				<a href="">Royal</a>
			</div>
			<div class="size">
				<span class="subtitile">Размер:</span>
				<div class="size-buttons">
					<button class="size-btn">140×205 см</button>
					<button class="size-btn">160×200 см</button>
					<button class="size-btn">180×200 см</button>
					<button class="size-btn">200×220 см</button>
					<button class="size-btn">240×250 см</button>
				</div>
			</div>
			<div class="buy">
			<!-- popup window -->
				<div class="product-popup modal-box" id="productPopup">
					<a href="#" class="x-form-button js-modal-close">×</a>
					<div class="product-popup-info">
						<h4>Товар добавлен к вашему заказу</h4>
						<div class="col-md-6">
							<div class="product-popup-info-thumb">
								<img src="assets/img/thumb/collections1.png">
							</div>
						</div>
						<div class="col-md-6">
							<div class="product-popup-info-meta">
								<h3>КГЕ «Аманда» КПБ «Гармоника» евро  (50×70-20 70×70-2) сатин-комфорт</h3>
								<span class="product-popup-qty">× 1 шт.</span>
								<span class="product-popup-cost">1 506 руб.</span>
							</div> 
						</div>
					</div>
					<div class="product-popup-buttons">
						<button class="btn">Продолжить покупки</button>
						<button class="btn">Оформить заказ</button>
					</div>
				</div>
			<!-- popup window END -->	
				<a href="#" class="bulk-buy js-open-modal" data-modal-id="productPopup" ><span>Купить Оптом</span></a>
				<a href="#" class="retail-buy"><span>Где купить в розницу</span></a>
			</div>
			<div class="desc-tabs">
				<ul>
					<li><a href="#description">Описание</a></li>
					<li><a href="#characterist">Характеристики</a></li>
				</ul>
				<div id="description" class="desc-tabs-info">
					<p>Оригинальная коллекция «Бамбук - Роял» с инновационным наполнителем из бамбукового волокна представлена в натуральном сатине-жаккарде. Уникальное бамбуковое волокно позволяет создать комфортные условия для сна, восстанавливающего силы.</p>

					<p>Важные потребительские свойства изделий коллекции «Бамбук - Роял»:</p>
					<ul>
						<li>экологически чистый природный материал</li>
						<li>высокие антибактериальные свойства</li>
						<li>гипоаллергенность: не вызывает раздражения и аллергии</li>
						<li>ощущение свежести: регулирует влажность и теплообмен</li>
						<li>долговечность: сохраняет свои первоначаль­ные свойства и форму после многократной эксплуатации</li>
					</ul>
				</div>
				<div id="characterist" class="desc-tabs-info">
					<ul>
						<li>экологически чистый природный материал</li>
						<li>высокие антибактериальные свойства</li>
						<li>гипоаллергенность: не вызывает раздражения и аллергии</li>
						<li>ощущение свежести: регулирует влажность и теплообмен</li>
						<li>долговечность: сохраняет свои первоначаль­ные свойства и форму после многократной эксплуатации</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<!-- #product-data-details END -->