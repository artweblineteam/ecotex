$(document).ready(function(){
  $('.slider').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000
  });
});

$('.testimonials-carousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    // autoplay: true,
    arrows: true,
    autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('#product-collections .top-sales-products').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    //autoplay: true,
    arrows: true,
    autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  //adaptiveHeight: true,
  asNavFor: '.slider-nav',
  responsive:[
  {
      breakpoint: 900,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true
      }
    }
  ]
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  arrows: false,
  centerMode: true,
  focusOnSelect: true,
  responsive:[
  {
      breakpoint: 900,
      settings: {
        slidesToShow: 0,
      }
    }
  ]
});

$(document).ready(function(){
  $('.slide-product').zoom({ on:'click' });;
});

$(document).ready(function(){
  $.fn.equivalent = function (){
    var $blocks = $(this),
    maxH    = $blocks.eq(0).height();

    $blocks.each(function(){
      maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH;
    });

    $blocks.height(maxH);
  }

  $('.testimonials-text').equivalent();
  $('.desc-tabs-info').equivalent();
  
  
  // $('.news-item').equivalent(); 
  
  if (window.innerWidth > 899) {
    window.onload = function(){
      $('.product-info-item').equivalent();
      $('.category-product-item').equivalent(); 
    }
  };
  if (window.innerWidth >700  ) {
      window.onload = function(){
         $('.footer-menu-widget').equivalent(); 
      }
  }

});


$( function() {
  $( ".desc-tabs" ).tabs();
});



$(function(){

  var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

  $('a[data-modal-id]').click(function(e) {
    e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 1);
    var modalBox = $(this).attr('data-modal-id');
    $('#'+modalBox).fadeIn($(this).data());
  });  


  $(".js-modal-close, .modal-overlay").click(function() {
    $(".modal-box, .modal-overlay").fadeOut(500, function() {
      $(".modal-overlay").remove();
    });
  });


});


$("i.icon-search").click(function () {
  $(this).toggleClass('open');
  $(".search-line").slideToggle("slow");
});


$('#size-filter').ddslick({
    background: '#ffffff',
    width: '210px',
});
$('#filler-filter').ddslick({
    background: '#ffffff',
    width: '134',
});
$('#collections-filter').ddslick({
    background: '#ffffff',
    width: '134',
});
$('#sort-by-filter').ddslick({
    background: '#ffffff',
    width: '180',
});



$(".nav-first").hover(function () {
  if ($(this).children('.nav-second').toggleClass('show')) {
    $('.inner-menus').each(function(){  
            var liItems = $(this);
            var Sum = 0;
            if(liItems.children('li').length >= 1){
             $(this).children('li').each(function(i, e){
                    Sum += $(e).outerWidth(true);
             });
            $(this).width(Sum+1+40);
       } 
    });
  }

});

/* mobile menu */
$(document).ready(function(){
  $('.hamb-menu-wrap').click(function () {
    $(this).toggleClass('open');
    $('.open-mobile-menu').toggleClass('hide');
  });
});

/*$(document).ready(function(){
   if ((window.innerWidth <= 899) && ($('.product-data').length)){
     window.onload = function(){
       var productData1 = document.getElementById("product-data-gallery");
       var productData2 = document.getElementById("product-data-details");
       productData1.style.marginTop = productData2.offsetHeight + "px";
       productData2.style.marginTop = -(productData1.offsetHeight + productData2.offsetHeight) + "px";
     }
   }
});*/

$(document).ready(function() {
  if (document.all && !document.addEventListener) {
        $('body').append('<div class="overlayIE"></div>');    
      }
});

$(document).ready(function() {
   
  $('.category-widget-title').click(function() {
    $('.category-widget-title').removeClass('on');
    $('.category-block').slideUp('normal');
    if($(this).next().is(':hidden') == true) {
      $(this).addClass('on');
      $(this).next().slideDown('normal');
     } 
   });
  $('.category-block').hide();
  if ($('.category-block').hasClass('active')){
    $('.category-block.active').prev().addClass('on');
    $('.category-block.active').slideDown('normal');
  }
});

$(document).ready(function() {
  $('.nav-first-mobile').click(function() {
    $('.nav-first-mobile').removeClass('on');
    $('.nav-second-mobile').slideUp('normal');
    if($(this).next().is(':hidden') == true) {
      $(this).addClass('on');
      $(this).next().slideDown('normal');
     } 
   });
  $('.nav-second-mobile').hide();
  // if ($('.category-block').hasClass('active')){
  //   $('.category-block.active').prev().addClass('on');
  //   $('.category-block.active').slideDown('normal');
  // }
});
 
$('.total-cart').hover(
  function() {
    var $this = $('.cart-box');
    $this.addClass('active');
  }, function() {
    var $this = $('.cart-box');
    $this.removeClass('active');
    setTimeout( function() { $this.removeClass('active');}, 300);
  }
);

$(document).ready(function(){
  if(window.innerWidth > 899) {
    window.onload = function(){
      $('.sticky').stickMe({
    topOffset: 145,
  });
    }
  }
  else{
    $('.sticky-mobile').stickMe({
    topOffset: 80,
  });
  }
});

