(function($){
	"use strict";
	var $window = $(window);

	
   /* menu items slider */
   
	
	var initSlider = function() {
		var slider = $('#nav');
		var innerWrap = slider.find('> ul.nav-primary');
		var slider_prev = $('.carousel-nav.next', slider);
		var slider_next = $('.carousel-nav.prev', slider);
		var menu_width = slider.width();
		var items_width = 0;
		
		$('.nav-first', slider).each(function() {
			var menu_item = $(this);

			items_width += menu_item.outerWidth(true);
			//items_width += ((menu_item.outerWidth(true))*2);
			
			if (items_width > menu_width) {
				menu_item.hide();
			} else {
				menu_item.show();
			}
		});
		if(items_width > menu_width) {
			slider.find('.carousel-nav').css('opacity', '1');
			slider_prev.unbind('click').on('click touchend',function() {
				var toHidePrev = innerWrap.find('.nav-first:visible:first');
				var toShowPrev = innerWrap.find('.nav-first:visible:last').next('.nav-first');
				if(toShowPrev.length > 0) {
					toHidePrev.hide('fast');
					toShowPrev.show('fast');
				}
			});
			slider_next.unbind('click').on('click touchend',function() {
				var toHideNext = innerWrap.find('.nav-first:visible:last');
				var toShowNext = innerWrap.find('.nav-first:visible:first').prev('.nav-first');
				if(toShowNext.length > 0) {
					toHideNext.hide('fast');
					toShowNext.show('fast');
				}
			});
		} else {
			slider.find('.carousel-nav').css('opacity', '0');
		}
	};
	
	$(window).on('load resize scroll', function() {
		setTimeout(function() {
			initSlider();
		}, 10);
	});
   
})(jQuery);