<div class="del"></div>
<section id="inner-header">
	<div class="container">
		<div class="row">
			<?php include ('components/breadcrumbs2.php') ?>
		</div>
	</div>
</section>
<!-- #lk-content -->
<section id="lk-content">
	<div class="container">
		<div class="row">
			<!-- .lk-navigation -->
			<div class="col-md-3 lk-navigation">
				<div class="row">
					<ul class="lk-nav">
						<li><a href="#" class="lk-nav-link lk-nav-my-orders active"><span>Корзина (+3)</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-my-akk"><span>Мой кабинет</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-personal-data	"><span>Личные данные</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-my-orders"><span>Мои заказы</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-mailing"><span>Рассылка</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-logout"><span>Выйти</span></a></li>
					</ul>
				</div>
			</div>
			<!-- .lk-navigation END -->
			<div class="col-md-9">
			<!-- .lk-inner -->
				<div class="row lk-inner">
					<!-- .shopping-cart -->
						<div class="shopping-cart">
							<div class="lk-titles">
								<h4 class="lk-inner-title">Товары к заказу</h4>
							</div>
							<div class="shopping-products">
								<table>
									<tr class="table-head">
										<th colspan="2">Наименование</th>
										<th class="product-cart-cost">Стоимость</th>
										<th class="product-cart-quantity">Количество</th>
										<th class="product-cart-total-cost">Сумма</th>
										<th class="product-cart-x">&nbsp;</th>
									</tr>
									<tr id="product1" class="product-item">
										<td class="product-cart-thumb">
											<img src="assets/img/thumb/collections1.png">
										</td>
										<td class="product-cart-title">
											КГЕ «Аманда» КПБ «Гармоника» евро (50×70-20 70×70-2) сатин-комфорт
										</td>
										<td class="product-cart-cost"> 2 949 pyб.</td>
										<td class="product-cart-quantity">
											<div class="quantity">
												<a href="#" class="qty-minus">-</a>
												<input type="" name="" value="273" class="input-quantity">
												<a href="#" class="qty-plus">+</a>
											</div>
										</td>
										<td class="product-cart-total-cost">20 003 949 руб.</td>
										<td class="product-cart-x"><a href="#">×</a></td>
									</tr>
									<tr id="product1" class="product-item">
										<td class="product-cart-thumb">
											<img src="assets/img/thumb/our-product1.png">
										</td>
										<td class="product-cart-title">
											ПБ57 Две Подушки «Бамбук-Роял» 50×70
										</td>
										<td class="product-cart-cost">1 500 руб.</td>
										<td class="product-cart-quantity">
											<div class="quantity">
												<a href="#" class="qty-minus">-</a>
												<input type="" name="" value="1" class="input-quantity">
												<a href="#" class="qty-plus">+</a>
											</div>	
										</td>
										<td class="product-cart-total-cost">1 500 руб.</td>
										<td class="product-cart-x"><a href="#">×</a></td>
									</tr>
									<tr id="product1" class="product-item">
										<td class="product-cart-thumb">
											<img src="assets/img/thumb/top-sales1.png">
										</td>
										<td class="product-cart-title">
											ПБ53 Подушка «Бамбук-Роял» 40×80
										</td>
										<td class="product-cart-cost">1 643 руб.</td>
										<td class="product-cart-quantity">
											<div class="quantity">
												<a href="#" class="qty-minus">-</a>
												<input type="" name="" value="12" class="input-quantity">
												<a href="#" class="qty-plus">+</a>
											</div>
										</td>
										<td class="product-cart-total-cost">19 506 руб.</td>
										<td class="product-cart-x"><a href="#">×</a></td>
									</tr>
								</table>
								<div class="checkout-buttons">
									<div class="col-md-6">
										<button class="btn clear-cart">Очистить корзину</button>
									</div>
									<div class="col-md-6">
										<div class="total">
											<span>Итого:</span><span> 30 659 849 руб.</span>
										</div>
										<button class="btn checkout-order">Оформить заказ</button>
									</div>
								</div>
							</div>
							
						</div>
					<!-- .shopping-cart END -->
					<div class="del"></div>
				</div>
			<!-- .lk-iiner END -->	
			</div>
		</div>
	</div>
</section>
<!-- #lk-inner END -->