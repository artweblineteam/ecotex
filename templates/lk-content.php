<div class="del"></div>
<section id="inner-header">
	<div class="container">
		<div class="row">
			<?php include ('components/breadcrumbs2.php') ?>
		</div>
	</div>
</section>
<!-- #lk-content -->
<section id="lk-content">
	<div class="container">
		<div class="row">
			<!-- .lk-navigation -->
			<div class="col-md-3 lk-navigation">
				<div class="row">
					<ul class="lk-nav">
						<li><a href="#" class="lk-nav-link lk-nav-my-akk active"><span>Мой кабинет</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-personal-data"><span>Личные данные</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-my-orders"><span>Мои заказы</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-mailing"><span>Рассылка</span></a></li>
						<li><a href="#" class="lk-nav-link lk-nav-logout"><span>Выйти</span></a></li>
					</ul>
				</div>
			</div>
			<!-- .lk-navigation END -->
			<div class="col-md-9">
			<!-- .lk-inner -->
				<div class="row lk-inner">
					<!-- .personal-data -->
						<div class="personal-data">
							<div class="lk-titles">
								<h4 class="lk-inner-title">Личные данные</h4>
								<a href="#" class="lk-link">изменить</a>
							</div>
							<div class="personal-data-col">
								<div class="personal-data-item">
									<span class="personal-data-title">Имя:</span>
									<span class="personal-data-info">Ярослав Константинович</span>
								</div>
								<div class="personal-data-item">
									<span class="personal-data-title">Юридическое лицо:</span>
									<span class="personal-data-info">ООО “Продажи текстиля”</span>
								</div>
							</div>
							
							<div class="personal-data-col">
								<div class="personal-data-item">
									<span class="personal-data-title">Статус договора:</span>
									<span class="personal-data-info not-signed">Договор не подписан <a href="#" class="download-contract">Скачать</a></span>
								</div>
								<div class="personal-data-item">
									<span class="personal-data-title">Ваши цены:</span>
									<span class="personal-data-info">Колонка 2</span>
								</div>
							</div>
						</div>
					<!-- .personal-data END -->
					<div class="del"></div>
					<!-- .last-orders -->
						<div class="last-orders">
							<div class="lk-titles">
								<h4 class="lk-inner-title">Последние заказы</h4>
								<a href="#" class="extra-link">Смотреть все</a>
							</div>	
							<table class="last-orders-table">
								<tr>
									<td class="last-orders-titles">
										Заказ №1289023
										<span class="last-order-meta">28.03.2017 14:05:29</span>
									</td>
									<td class="last-orders-date">28.10.2017</td>
									<td class="last-orders-amount">2 891 949 руб.</td>
									<td class="last-orders-status">Выстален счет</td>
									<td class="last-orders-more">
										<button class="btn">Подробнее</button>
									</td>
								</tr>
								<tr>
									<td class="last-orders-titles">
										Заказ №902834
										<span class="last-order-meta">28.03.2017 14:05:29</span>
									</td>
									<td class="last-orders-date">28.10.2017</td>
									<td class="last-orders-amount">2 891 949 руб.</td>
									<td class="last-orders-status">Доставлено</td>
									<td class="last-orders-more">
										<button class="btn">Подробнее</button>
									</td>
								</tr>
								<tr> 
									<td class="last-orders-titles">
										Заказ №12578
										<span class="last-order-meta">28.03.2017 14:05:29</span>
									</td>
									<td class="last-orders-date">28.10.2017</td>
									<td class="last-orders-amount">2 891 949 руб.</td>
									<td class="last-orders-status canceled">Отменен</td>
									<td class="last-orders-more">
										<button class="btn">Подробнее</button>
									</td>
								</tr>
							</table>
						</div>
					<!-- .last-orders END -->
					<div class="del"></div>
					<!-- .useful -->
						<div class="useful">
							<div class="lk-titles">
								<h4 class="lk-inner-title">Полезная информация</h4>
								<a href="#" class="extra-link">Все новости</a>
							</div>
							<table>
								<tr>
									<td class="useful-title">Как правильно пользоваться личным кабинетом</td>
									<td class="useful-date">31.12.2017</td>
									<td class="useful-more"><button class="btn">Подробнее</button></td>
								</tr>
								<tr>
									<td class="useful-title">Работа компании на новогодние праздники</td>
									<td class="useful-date">31.12.2017</td>
									<td class="useful-more"><button class="btn">Подробнее</button></td>
								</tr>
								<tr>
									<td class="useful-title">Новые поступления в декабре</td>
									<td class="useful-date">31.12.2017</td>
									<td class="useful-more"><button class="btn">Подробнее</button></td>
								</tr>
							</table>
						</div>
					<!-- .useful END -->
					<div class="del"></div>
					<!-- .catalogue -->
						<div class="catalogue">
							<div class="lk-titles">
								<h4 class="lk-inner-title">Каталоги и прайс-листы</h4>
							</div>
							<table>
								<tr>
									<td class="catalogue-title"><span class="catalogue-icon-pdf">Каталог Одеяла, подушки и полотенца</span></td>
									<td class="catalogue-date">31.12.2017</td>
									<td class="catalogue-more"><button class="btn">Скачать</button></td>
								</tr>
								<tr>
									<td class="catalogue-title"><span class="price-list-icon-xls">Прайс-лист Постельное белье</span></td>
									<td class="catalogue-date">31.12.2017</td>
									<td class="catalogue-more"><button class="btn">Скачать</button></td>
								</tr>
								<tr>
									<td class="catalogue-title"><span class="catalogue-icon-pdf">Каталог Постельное белье</span></td>
									<td class="catalogue-date">31.12.2017</td>
									<td class="catalogue-more"><button class="btn">Скачать</button></td>
								</tr>
							</table>
						</div>
					<!-- .catalogue END -->
				</div>
			<!-- .lk-iiner END -->	
			</div>
		</div>
	</div>
</section>
<!-- #lk-inner END -->