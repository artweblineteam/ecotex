<?php include ('components/stunning-header.php');?>
<!-- #category -->
<section id="category">
	<div class="container">
		<div class="row">
		<?php include('components/category-sidebar.php');?>
		<!-- .category-content -->
			<div class="col-md-9 category-content">
				<div class="row">
					<?php include('components/category-filters.php');?>
				</div>
				<div class="row">
					<?php include('components/category-products.php');?>
				</div>
				<div class="row">
					<?php include('components/pagination.php');?>
				</div>
			</div>
		<!-- .category-content END -->	
		</div>
	</div>
</section>
<!-- #category END -->

<div class="del"></div>

<?php include('components/seo-text2.php');?>