<div class="del"></div>
<section id="inner-header">
	<div class="container">
		<div class="row">
			<?php include ('components/breadcrumbs3.php') ?>
		</div>
	</div>
</section>
<!-- #blog-page-title -->
	<section id="blog-page-title">
		<div class="container">
			<div class="row">
				<h1 class="big-title">Наши новости</h1>
			</div>
		</div>
	</section>
<!-- #blog-page-title END-->

<!-- #banner-slider -->
	<section id="banner-slider">
		<div class="container">
			<div class="row">
				<div class="slider">
					<div class="slider-item">
						<img src="assets/img/slider/blog-slider.jpg">
						<h1 class="slider-title">Всем подарки и скидки <br>ко Дню СВ. НИколая</h1>
						<button class="btn banner-btn">Смотреть</button>
					</div>
					<div class="slider-item">
						<img src="assets/img/slider/blog-slider.jpg">
						<h1 class="slider-title">Всем скидки <br>и подарки!</h1>
						<button class="btn banner-btn">Смотреть</button>
					</div>
					<div class="slider-item">
						<img src="assets/img/slider/blog-slider.jpg">
						<h1 class="slider-title">Акционные <br>предложения!</h1>
						<button class="btn banner-btn">Смотреть</button>
					</div>
					<div class="slider-item">
						<img src="assets/img/slider/blog-slider.jpg">
						<h1 class="slider-title">Домашний <br>текстиль!</h1>
						<button class="btn banner-btn">Смотреть</button>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- #banner-slider END -->

<!-- #all-posts -->
	<section id="all-posts">
		<div class="container">
			<div class="row">
				<div class="post-preview">
					<div class="post-thumbnail">
						<img src="assets/img/thumb/blog1.jpg">
					</div>
					<div class="post-info">
						<h4 class="post-title"><a href="#">Орнамент «аргайл» и ромбы в интерьере</a></h4>
						<p class="post-excerpt">Торгово-производственная компания «Экотекс» является отечественным производителем домашнего текстиля, успешно работает и динамично развивается на российском рынке.</p>
						<span class="news-meta">
							Декабрь 25, 2018
						</span>
					</div>
				</div>
				<div class="post-preview">
					<div class="post-thumbnail">
						<img src="assets/img/thumb/blog2.jpg">
					</div>
					<div class="post-info">
						<h4 class="post-title"><a href="#">Все оттенки осени</a></h4>
						<p class="post-excerpt">Торгово-производственная компания «Экотекс» является отечественным производителем домашнего текстиля, успешно работает и динамично развивается на российском рынке.</p>
						<span class="news-meta">
							Декабрь 25, 2018
						</span>
					</div>
				</div>
				<div class="post-preview">
					<div class="post-thumbnail">
						<img src="assets/img/thumb/blog3.jpg">
					</div>
					<div class="post-info">
						<h4 class="post-title"><a href="#">Дипломат — новая коллекция полотенец</a></h4>
						<p class="post-excerpt">Торгово-производственная компания «Экотекс» является отечественным производителем домашнего текстиля, успешно работает и динамично развивается на российском рынке.</p>
						<span class="news-meta">
							Декабрь 25, 2018
						</span>
					</div>
				</div>
				<div class="post-preview">
					<div class="post-thumbnail">
						<img src="assets/img/thumb/blog4.jpg">
					</div>
					<div class="post-info">
						<h4 class="post-title"><a href="#">Привлекательная геометрия</a></h4>
						<p class="post-excerpt">Торгово-производственная компания «Экотекс» является отечественным производителем домашнего текстиля, успешно работает и динамично развивается на российском рынке.</p>
						<span class="news-meta">
							Декабрь 25, 2018
						</span>
					</div>
				</div>
				<div class="post-preview">
					<div class="post-thumbnail">
						<img src="assets/img/thumb/blog5.jpg">
					</div>
					<div class="post-info">
						<h4 class="post-title"><a href="#">Сквозь века</a></h4>
						<p class="post-excerpt">Торгово-производственная компания «Экотекс» является отечественным производителем домашнего текстиля, успешно работает и динамично развивается на российском рынке.</p>
						<span class="news-meta">
							Декабрь 25, 2018
						</span>
					</div>
				</div>
				<div class="more-posts">
					<button class="btn more-news">Больше новостей</button>
				</div>
			</div>
		</div>
	</section>
<!-- #all-posts END -->
