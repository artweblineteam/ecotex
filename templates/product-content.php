<div class="del"></div>
<section id="inner-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php include ('components/breadcrumbs1.php') ?>
			</div>
		</div>
	</div>
</section>
<section id="product-card">
	<div class="product-data">
		<div class="container">
			<div class="row">
				<?php include ('components/product-data-gallery.php') ?>
				<?php include ('components/product-data-details.php') ?>
			</div>
		</div>
	</div>
	<?php include ('components/product-bigimage.php') ?>
	<?php include ('components/product-info.php') ?>

</section>
<div class="del"></div>
<?php include ('components/our-features-product.php') ?>
<?php include ('components/product-collections.php') ?>
