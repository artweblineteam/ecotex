<!-- footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="footer-menus">
					<div class="col-md-10 col-md-offset-1">
						<div class="footer-menu-widget col-md-3">
						<h5>Постельное белье</h5>
						<ul class="footer-menu">
							<li class="footer-menu-item"><a href="#">Сатин-жаккар</a></li>
							<li class="footer-menu-item"><a href="#">Поплин</a></li>
							<li class="footer-menu-item"><a href="#">Сатин 3Д</a></li>
							<li class="footer-menu-item"><a href="#">Сатин-Премиум</a></li>
						</ul>
					</div>
					<div class="footer-menu-widget col-md-3">
						<h5>Подушки</h5>
						<ul class="footer-menu">
							<li class="footer-menu-item"><a href="#">Бамбук</a></li>
							<li class="footer-menu-item"><a href="#">Верблюжья шерсть</a></li>
							<li class="footer-menu-item"><a href="#">Кукуруза</a></li>
							<li class="footer-menu-item"><a href="#">Крапива</a></li>
						</ul>
					</div>
					<div class="footer-menu-widget col-md-3">
						<h5>Одеяла</h5>
						<ul class="footer-menu">
							<li class="footer-menu-item"><a href="">Бамбук</a></li>
							<li class="footer-menu-item"><a href="">Верблюжья шерсть</a></li>
							<li class="footer-menu-item"><a href="">Кукуруза</a></li>
							<li class="footer-menu-item"><a href="">Крапива</a></li>
							<li class="footer-menu-item"><a href="">Крапива</a></li>
						</ul>
					</div>
					<div class="footer-menu-widget col-md-3">
						<h5>Наматрасники</h5>
						<ul class="footer-menu">
							<li class="footer-menu-item"><a href="">Бамбук</a></li>
							<li class="footer-menu-item"><a href="">Верблюжья шерсть</a></li>
							<li class="footer-menu-item"><a href="">Кукуруза</a></li>
							<li class="footer-menu-item"><a href="">Кукуруза</a></li>
							<li class="footer-menu-item"><a href="">Крапива</a></li>
						</ul>
					</div>
					<div class="footer-menu-widget col-md-3">
						<h5>Пледы</h5>
						<ul class="footer-menu">
							<li class="footer-menu-item"><a href="">Детские</a></li>
							<li class="footer-menu-item"><a href="">Махровые</a></li>
						</ul>
					</div>
					<div class="footer-menu-widget col-md-3">
						<h5>Махровые изделия</h5>
						<ul class="footer-menu">
							<li class="footer-menu-item"><a href="#">Полотенца</a></li>
							<li class="footer-menu-item"><a href="#">Простыни</a></li>
						</ul>
					</div>
					<div class="footer-menu-widget col-md-3">
						<h5>О компании</h5>
						<ul class="footer-menu">
							<li class="footer-menu-item"><a href="#">Наши награды</a></li>
							<li class="footer-menu-item"><a href="#">Наше производство</a></li>
							<li class="footer-menu-item"><a href="#">Наше производство</a></li>
						</ul>
					</div>
					<div class="footer-social col-md-3">
						<h5>Ищите нас в соцсетях</h5>
						<ul class="footer-menu">
							<li class="footer-social-item"><a href="#" class="social-link vk"></a></li>
							<li class="footer-social-item"><a href="#" class="social-link fb"></a></li>
							<li class="footer-social-item"><a href="#" class="social-link ok"></a></li>
							<li class="footer-social-item"><a href="#" class="social-link yt"></a></li>
						</ul>
					</div>
					</div>

				</div>
				<div class="del"></div>
				<div class="subfooter">
					<div class="copy">© 2000 – 2016 Ecotex</div>
					<div class="footer-tel">
						<a href="tel:+74952253453">+7 (495) 225-34-53</a>
						<a href="tel:88002253453">8-800-225-34-53</a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- footer END -->
</body>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="../../assets/js/jquery.ddslick.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript" src="assets/js/slick.js"></script>
<script type="text/javascript" src="assets/js/jquery.zoom.js"></script>
<script type="text/javascript" src="assets/js/sticky-header.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/scroll-menu.js"></script>

</html>