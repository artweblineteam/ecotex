<!DOCTYPE html>
<html>
<head>
	<title>ECOTEX</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta http-equiv="imagetoolbar" content="no"/>
	<meta http-equiv="msthemecompatible" content="no"/>
	<meta name="HandheldFriendly" content="True"/>
	<meta name="format-detection" content="telephone=no"/>
	<meta name="format-detection" content="address=no"/>
	<meta http-equiv="cleartype" content="on"/>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="stylesheet" type="text/css" href="assets/css/grid.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="assets/css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">


</head>
<body>
	<!-- header -->
	<header class="">
		<div class="top-header sticky">
			<div class="top-menu">
				<div class="icon-menu">
					<span class="hamburger-icon"></span>
				</div>
			</div>
			<div class="top-links">
				<ul>
					<li><a href="#">О компании</a></li> 
					<li><a href="#">Где купить</a></li>
					<li><a href="#">Контакты</a></li>
				</ul>
			</div>
			<div class="top-info">
				<a href="tel:+74952253453" class="tel">+7 (495) 225-34-53</a>
				<a href="tel:88002253453" class="tel">8-800-225-34-53</a>
				<span class="info text-color-light">Работаем с 10 до 21</span>
			</div>
			
			<div class="sign-up">
				<a href="#">Регистрация</a>
			</div>
			<div class="sign-in">
				<a href="#">Личный кабинет</a>
			</div>
			<?php include ('components/callback.php') ?>	
		</div>
		<div class="bottom-header">
			<div class="container">
				<div class="row">
					<div class="logo">
						<a href="#">
							<img src="assets/img/logo.png" alt="logo Ecotex">
						</a>
					</div>
					<div class="total-cart">
						<a href="#" class="cart-content">
							<span class="cart-item">
								<i class="icon-cart"></i>
							</span>
							<span class="cart-details">0</span>
						</a>
						<div class="cart-box">
							<div class="cart-box-content">
								<span class="shoping-cart-content"> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
								<div class="shopping-buttons">
									<button class="btn">Просмотреть корзину</button>
									<button class="btn">Оформить заказ</button>
								</div>
							</div>
						</div>
					</div>
					<div class="search">
						<i class="icon-search"></i>
						<div class="search-line">
							<div class="container">
								<div class="row">
									<form class="search-form" action="#">
										<input type="text" name="search" placeholder="Поиск по сайту, введите, что бы Вы хотели найти">
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="nav-fluid">
						<nav class="nav" id="nav">
							<ul class="nav-primary">
								<li class="nav-first">
									<a href="#" class="nav-first-link">Постельное белье</a>
									<div class="nav-second">
										<ul class="inner-menus">
											<li class="submenu">
												<h5 class="submenu-title">Наполнители</h5>
												<ul class="submenu-items">
													<li><a href="#">Файбер</a></li>
													<li><a href="#">Бамбуковое волокно</a></li>
													<li><a href="#">Кашемир</a></li>
												</ul>
											</li>
											<li class="submenu">
												<h5 class="submenu-title">Размер</h5>
												<ul class="submenu-items">
													<li><a href="#">160 × 200 см</a></li>
													<li><a href="#">180 × 200 см</a></li>
													<li><a href="#">200 × 200 см</a></li>
													<li><a href="#">200 × 220 см</a></li>
												</ul>
											</li>
											<li class="submenu">
												<h5 class="submenu-title">Ткань чехла</h5>
												<ul class="submenu-items">
													<li><a href="#">Сатин</a></li>
													<li><a href="#">Тик</a></li>
													<li><a href="#">100% хлопок</a></li>
												</ul>
											</li>
											<li class="submenu">
												<h5 class="submenu-title">Коллекция</h5>
												<ul class="submenu-items">
													<li><a href="#">Роял</a></li>
													<li><a href="#">Премиум</a></li>
													<li><a href="#">Комфорт</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>           
								<li class="nav-first">
									<a href="#" class="nav-first-link">Простыни на резинке</a>
									<div class="nav-second">
										<ul class="inner-menus">
											<li class="submenu">
												<h5 class="submenu-title">Наполнители</h5>
												<ul class="submenu-items">
													<li><a href="#">Файбер</a></li>
													<li><a href="#">Бамбуковое волокно</a></li>
													<li><a href="#">Кашемир</a></li>
												</ul>
											</li>
										</ul>
									</div>		
								</li>      
								<li class="nav-first">
									<a href="#" class="nav-first-link">Одеяла</a>
									<div class="nav-second">
										<ul class="inner-menus">
											<li class="submenu">
												<h5 class="submenu-title">Наполнители</h5>
												<ul class="submenu-items">
													<li><a href="#">Файбер</a></li>
													<li><a href="#">Бамбуковое волокно</a></li>
													<li><a href="#">Кашемир</a></li>
												</ul>
											</li>
											<li class="submenu">
												<h5 class="submenu-title">Размер</h5>
												<ul class="submenu-items">
													<li><a href="#">160 × 200 см</a></li>
													<li><a href="#">180 × 200 см</a></li>
													<li><a href="#">200 × 200 см</a></li>
													<li><a href="#">200 × 220 см</a></li>
												</ul>
											</li>
										</ul>
									</div>	
								</li>      
								<li class="nav-first">
									<a href="#" class="nav-first-link">Подушки</a>
									<div class="nav-second">
										<ul class="inner-menus">
											<li class="submenu">
												<h5 class="submenu-title">Наполнители</h5>
												<ul class="submenu-items">
													<li><a href="#">Файбер</a></li>
													<li><a href="#">Бамбуковое волокно</a></li>
													<li><a href="#">Кашемир</a></li>
												</ul>
											</li>
											<li class="submenu">
												<h5 class="submenu-title">Ткань чехла</h5>
												<ul class="submenu-items">
													<li><a href="#">Сатин</a></li>
													<li><a href="#">Тик</a></li>
													<li><a href="#">100% хлопок</a></li>
												</ul>
											</li>
											<li class="submenu">
												<h5 class="submenu-title">Коллекция</h5>
												<ul class="submenu-items">
													<li><a href="#">Роял</a></li>
													<li><a href="#">Премиум</a></li>
													<li><a href="#">Комфорт</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>      
								<li class="nav-first"><a href="#" class="nav-first-link">Наматрасники</a></li> 
								<li class="nav-first"><a href="#" class="nav-first-link">Пледы</a></li>
								<li class="nav-first"><a href="#" class="nav-first-link">Махровые изделия</a></li>
								<li class="nav-first"><a href="#" class="nav-first-link">Наволочки декоративные</a></li>
							</ul>
							<i class="carousel-nav prev icon-arrow"></i>
							<i class="carousel-nav next icon-arrow"></i>
						</nav>	
					</div>
				</div>
			</div>
		</div>
<?php 
	include ('header-mobile.php');
	?>
	</header>
	<!-- header END -->